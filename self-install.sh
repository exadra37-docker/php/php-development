#!/bin/bash
# @package exadra37-docker/php-development
# @link    https://gitlab.com/u/exadra37-docker/php-development
# @since   2016/10/14
# @license MIT
# @author  Exadra37(Paulo Silva) <exadra37ingmailpointcom>
#
# Social Links:
# @link    Auhthor:  https://exadra37.com
# @link    Github:   https://github.com/Exadra37
# @link    Linkedin: https://uk.linkedin.com/in/exadra37
# @link    Twitter:  https://twitter.com/Exadra37


########################################################################################################################
# Functions
########################################################################################################################

    function Export_Path ()
    {
        local home_dir=/home/"${USER}"
        local home_bin_dir="${home_user}/bin"

        if [ -f "${home_dir}"/.profile ]
            then
                echo "export PATH=${home_bin_dir}:${PATH}"  >> "${home_dir}/.profile"
        fi

        if [ -f "${home_dir}"/.bash_profile ]
            then
                echo "export PATH=${home_bin_dir}:${PATH}"  >> "${home_dir}/.bash_profile"
        fi

        if [ -f "${home_dir}"/.zshrc ]
            then
                echo "export PATH=${home_bin_dir}:${PATH}"  >> "${home_dir}/.zshrc"
        fi

        return 0
    }

########################################################################################################################
# Variables
########################################################################################################################

    package_manager_dir=/home/"${USER}"/bin/.vendor/exadra37-bash/package-manager
    self_dir=/home/"${USER}"/bin/.vendor/exadra37-docker/php-development


########################################################################################################################
# Execution
########################################################################################################################

    mkdir -p ${package_manager_dir} &&
        curl -L https://gitlab.com/exadra37-bash/package-manager/repository/archive.tar.gz | tar -zxvf - -C ${package_manager_dir} --strip-components=1
    
    mkdir -p ${self_dir} &&
        curl -L https://gitlab.com/exadra37-docker/php-development/repository/archive.tar.gz | tar -zxvf - -C ${self_dir} --strip-components=1

    bash ${package_manager_dir}/src/package-manager.sh install

    ln -s "${self_dir}"/src/docker-execute.sh /home/"${USER}"/bin/dexec
    ln -s "${self_dir}"/src/docker-stop.sh /home/"${USER}"/bin/dstop
    ln -s "${self_dir}"/src/docker-run.sh  /home/"${USER}"/bin/drun

    Export_Path

    printf "\n\nPlease reload your shell or open a new Terminal and then type on it: drun\n\n"
