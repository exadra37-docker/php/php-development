#!/bin/bash
# @package exadra37-docker/php-development
# @link    https://gitlab.com/u/exadra37-docker/php-development
# @since   2016/10/14
# @license MIT
# @author  Exadra37(Paulo Silva) <exadra37ingmailpointcom>
#
# Social Links:
# @link    Auhthor:  https://exadra37.com
# @link    Github:   https://github.com/Exadra37
# @link    Linkedin: https://uk.linkedin.com/in/exadra37
# @link    Twitter:  https://twitter.com/Exadra37

########################################################################################################################
# Sourcing
########################################################################################################################

    script_path=$(dirname $(readlink -f $0))

    source "${script_path}"/../vendor/exadra37-bash/pretty-print/src/sourcing/pretty-print-trait.source.sh
    source "${script_path}"/../src/functions/docker-validator.source.sh


########################################################################################################################
# Get Required Argument
########################################################################################################################

    image_name="${1}"


########################################################################################################################
# Validations
########################################################################################################################

    Abort_If_Argument_Was_Not_Provided "${image_name}" "The Image Name must be provided as the first argument."

    Abort_If_Docker_Container_Is_Not_Running "${image_name}"


########################################################################################################################
# Execution
########################################################################################################################

    # Stop a running container
    sudo docker stop ${image_name}
