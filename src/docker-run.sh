#!/bin/bash
# @package exadra37-docker/php-development
# @link    https://gitlab.com/u/exadra37-docker/php-development
# @since   2016/10/14
# @license MIT
# @author  Exadra37(Paulo Silva) <exadra37ingmailpointcom>
#
# Social Links:
# @link    Auhthor:  https://exadra37.com
# @link    Github:   https://github.com/Exadra37
# @link    Linkedin: https://uk.linkedin.com/in/exadra37
# @link    Twitter:  https://twitter.com/Exadra37

########################################################################################################################
# Sourcing
########################################################################################################################

    script_path=$(dirname $(readlink -f $0))

    source "${script_path}"/../vendor/exadra37-bash/pretty-print/src/sourcing/pretty-print-trait.source.sh
    source "${script_path}"/../src/functions/docker-validator.source.sh


########################################################################################################################
# Variables
########################################################################################################################

    # Optional - A Docker Image for Php Development as per found in Docker Hub
    # @link https://hub.docker.com/_/php/
    available_images_tags="php:5.6 php:7.0 php:7.1"

    # Optional Argumment: -i
    #
    # The default PHP Image to use from $available_images_tags
    php_image_tag="php:7.0"

    # Optional Argument: -n
    #
    # By default the container name is generated from $php_container_tag:
    #   * An container tag php:7.1 will originate the container name php-7.1.
    #
    # To specify our custom container name:
    #   * -n custom-name
    #
    # To let Docker auto generate the container name for us we just:
    #   * -n auto
    container_name=""

    # Optional - A valid service to execute on the container after we start it
    #   * zsh
    #   * bash
    #   * composer install
    #   * php -a
    command_to_execute=""

    # Optional Argument: -w
    #
    # By default we map the current current dir from where we run the command to the container dir /opt
    #
    # To give a different map just do:
    #   * -w dir-in-host:dir-in-container
    work_dir="${PWD}:/opt"

    # Optional Argument: -l
    #
    # Php Server log for running container
    chrome_log_path="/home/$USER/.exadra37/docker/php-development/chrome"


########################################################################################################################
# Get Optional Arguments
########################################################################################################################

    while getopts ':i:n:p:w:c:l:' flag; do
      case "${flag}" in
        i) php_image_tag="${OPTARG:-${php_image_tag}}";; # must be a valid one as per $available_images_tags
        n) container_name="${OPTARG:-${php_image_tag/:/-}}";;
        p) ports="${OPTARG}";;
        w) work_dir="${OPTARG:-${work_dir}}";;
        c) command_to_execute="${OPTARG}";;
        l) chrome_log_path="${OPTARG:-${chrome_log_path}}";;
        \?) printf "\n\n \e[101m Option -$OPTARG is not supported. \e[0m \n\n" && exit 1;;
        :) printf "\n\n \e[101m Option -$OPTARG requires a value. \e[0m \n\n" && exit 1;;
      esac
    done


########################################################################################################################
# Defaults
########################################################################################################################

    # If we pass 'auto' as the container name it means we want Docker to generate it for us,
    #   therefore we must set it to empty.
    if [ "auto" == "${container_name}" ]
        then
            container_name=""

    # Set container name from image tag
    elif [ -z "${container_name}" ]
        then
            container_name=${php_image_tag/:/-}
    fi

    # If no ports map have been provided we will do it
    if [ -z ${ports} ]
        then
            case "${php_image_tag}" in
                "php:5.6") ports="8056:80";;
                "php:7.0") ports="8070:80";;
                "php:7.1") ports="8071:80";;
                *) Print_Fatal_Error "Image ${php_image_tag} is not supported.";;
            esac
    fi

    chrome_log_file="${chrome_log_path}/${container_name}.log"


########################################################################################################################
# Validations
########################################################################################################################

    [ -d "${chrome_log_path}" ] || mkdir -p "${chrome_log_path}"
    [ -f "${chrome_log_file}" ] || touch "${chrome_log_file}"

    Abort_If_Argument_Was_Not_Provided "${container_name}" "The Image Name must be provided as the first argument."

    Abort_If_Docker_Container_Exists "${container_name}"

    Abort_If_Host_Port_Are_Already_Assigned_To_Container "${ports}"

    Abort_If_Host_Port_Is_Being_Used "${ports}"


########################################################################################################################
# Execution
########################################################################################################################

    # Build Docker Image
    sudo docker build -t ${php_image_tag} "${script_path}"/../build/${php_image_tag}

    # Run Docker Image in Detached mode, haka in background
    sudo docker run -d --name="${container_name}" -p ${ports} -v "${work_dir}" ${php_image_tag}

    # Inform Url to reach server on the browser
    url=http://localhost:${ports%%:*}
    Print_Info "Built in Server started. Chrome should have now a tab open on ${url}"
    Print_Empty_Line
    Print_Label_With_Text "To see logs for the open chrome tab see: " "${chrome_log_file}"

    # If we do not have an index.php we will need to add one in order we can test the Php Server
    [ -f "${work_dir%%:*}/index.php" ] || echo '<?php phpinfo();' > "${work_dir%%:*}/index.php"

    # Test Php Server in Google Chrome Browser
    nohup google-chrome -new-tab ${url} > "${chrome_log_file}" 2>&1 & # ampersand on the end will run this on shell background

    # If we have a service to run on the Container, like the ZSH" Shell we will execute attach it to the container
    if [ ! -z "${command_to_execute}" ]
        then

            # if the image name was auto generated we need to get it in order
            #  we can attach the service we want to execute
            [ -z ${container_name} ] && container_name=$(docker ps -l --format "{{.Names}}")

            bash "${script_path}"/docker-execute.sh ${container_name} ${command_to_execute}

        else

            Print_Empty_Line
            Print_Label_With_Text "To go inside ${container_name} container execute in your shell:" " dexec ${container_name} zsh" "green"
            Print_Empty_Line
    fi
