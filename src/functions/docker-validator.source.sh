#!/bin/bash
# @package exadra37-docker/php-development
# @link    https://gitlab.com/u/exadra37-docker/php-development
# @since   2016/10/14
# @license MIT
# @author  Exadra37(Paulo Silva) <exadra37ingmailpointcom>
#
# Social Links:
# @link    Auhthor:  https://exadra37.com
# @link    Github:   https://github.com/Exadra37
# @link    Linkedin: https://uk.linkedin.com/in/exadra37
# @link    Twitter:  https://twitter.com/Exadra37


########################################################################################################################
# Functions
########################################################################################################################

    function Print_Port_Number_Match()
    {
        local port_number=${1%%:*}

        netstat -ntpl | grep -n --colour ":${port_number}"

        return 0
    }

    function Print_Docker_Container_Match_When_Present()
    {
        local container_name=${1}

        sudo docker ps -a | grep -n --colour "${container_name}"

        return 0
    }

    function Print_Docker_Container_Match_When_Active()
    {
        local container_name=${1}

        sudo docker ps | grep -n --colour "${container_name}"

        return 0
    }

    function Print_Docker_Container_Port_Map_Match()
    {
        local port_map=${1}

        sudo docker ps -a | grep -n --colour "${port_map/:/->}"

        return 0
    }

    function Is_Docker_Container_Present()
    {
        local container_name=${1}

        local match_count=$( sudo docker ps -a --format "{{.Names}}" | grep -c "^${container_name}$" )

        [ ${match_count} -gt 0 ] && return 0 || return 1
    }

    function Is_Port_Number_Free()
    {
        local port_number=${1%%:*}

        local match_count=$( netstat -ntpl | grep -c ":${port_number}" )

        [ ${match_count} -gt 0 ] && return 0 || return 1
    }

    function Is_Host_Port_Assigned_To_Docker_Container()
    {
        local port_map=${1}

        local match_count=$( sudo docker ps -a | grep -c "${port_map/:/->}" )

        [ ${match_count} -gt 0 ] && return 0 || return 1
    }

    function Is_Docker_Container_Active()
    {
        local container_name=${1}

        local match_count=$( sudo docker ps --format "{{.Names}}" | grep -c "^${container_name}$" )

        [ ${match_count} -gt 0 ] && return 0 || return 1
    }

    function Abort_If_Docker_Container_Is_Not_Running()
    {
        local container_name=${1}

        if ! Is_Docker_Container_Active ${container_name}
            then
                sudo docker ps

                Print_Critical_Error "Provide a name for a container that is running."

                Print_Bold_Comment "Example: dexec php-7.0 zsh"

                exit 1;
        fi
    }

    function Abort_If_Docker_Container_Exists()
    {
        local container_name=${1}

        if Is_Docker_Container_Present ${container_name}
            then
                Print_Docker_Container_Match_When_Present "${container_name}"

                Print_Critical_Error "The container name ${container_name} already Exists. Please provider another one."

                Print_Bold_Comment "Example for Custom Name: dexec -n php-7.0"

                Print_Bold_Comment "Example for Auto Generated name: dexec -n auto"

                exit 1;
        fi
    }

    function Abort_If_Host_Port_Are_Already_Assigned_To_Container()
    {
        local port_map=${1}

        if Is_Host_Port_Assigned_To_Docker_Container ${port_map}
            then
                Print_Docker_Container_Port_Map_Match ${port_map}

                Print_Fatal_Error "Port Map ${port_map} is already in use by another container."
        fi
    }

    function Abort_If_Host_Port_Is_Being_Used()
    {
        local port_map=${1}

        if Is_Port_Number_Free ${port_map}
            then
                Print_Port_Number_Match ${port_map}

                Print_Fatal_Error "Port Map ${port_map} is already in use by another program."
        fi
    }

    function Abort_If_Argument_Was_Not_Provided()
    {
        local argument="${1}"
        local message="${2}"

        # Check i container name exists
        if [ -z "${argument}" ]
            then
                sudo docker ps

                Print_Critical_Error "${message}"

                Print_Bold_Comment "Example: dexec php-7.0 zsh"

                exit 1;
        fi

        return 0;
    }
