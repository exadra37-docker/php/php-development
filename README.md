# PHP DEVELOPMENT WITH DOCKER

### DEPRECATED

Use isntead [Php Docker Stack](https://gitlab.com/exadra37-docker/php/docker-stack)


#### Self Install

This package requires other packages, that are auto managed.

Please use the self installer for free hassle installation. You can check the installer [here](https://gitlab.com/exadra37-docker/php-development/raw/master/self-install.sh).

```bash
curl -L https://gitlab.com/exadra37-docker/php-development/raw/master/self-install.sh | bash -s
```

#### How to Use

You may need to prefix the commands with `sudo`.


##### To Create the Docker Container with PHP Server in last stable release

Use like `drun` to run with defaults

```bash
drun # same as bash drun -i php:7.0 -n php:7.0 -p 8070:80 -w .:/opt -l /var/log/docker-container/php-7.0.log
```

##### To create the Docker Container with PHP 5.6 Server

Use like `bash drun -i <docker-image-name>`

```bash
drun -i php:5.6
```

##### To create the Docker Container with last PHP Release Candidate Version

Use like `drun -i <docker-image-name>`

```bash
drun -i php:7.1
```

##### Create a Docker Container with a PHP Server and at same time run a command inside it

Use like `drun -i <docker-image-name> -n <container-name> -p <host-port:container-port> -w <project-dir:container-dir> -l <host-path-to-log> -c <command-to-execute>`

```bash
drun -i php:7.1 -n php7-dev -p 8887:80 -w /project-dir:/opt -l /var/log/docker-container/php7-dev.log -c bash
```

##### Execute a Command Inside a Docker Container

Use like `dexec <container-name> <command-to-execute>`

```bash
dexec php7-dev bash
```

##### Stop a Docker Container

Use like `bash dstop <container-name>`

```bash
dstop php7-dev
```
